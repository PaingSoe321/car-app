@extends('layouts.app')

@section('title')
    Cars
@endsection

@section('content')
    <div class="d-flex justify-content-between align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h2>Cars</h2>
        <div class="btn-toobar mb-2 mb-md-0">
            <a href="/cars/create" class="btn btn-sm btn-primary">Add New Car</a>
        </div>
    </div>
    <div class="table-responsive shadow rounded">
        <table class="table table-sm table-striped table-hover">
            <tr>
                <th>#</th>
                <th>Make</th>
                <th>Model</th>
                <th>Produced_On</th>
                <th class="text-center">Action</th>
            </tr>
            @foreach ($cars as $car)
            <tr>
                <td>{{ $loop->index }}</td>
                <td>{{ $car->make }}</td>
                <td>{{ $car->model }}</td>
                <td>{{ $car->produced_on }}</td>
                <td class="text-center">
                    <a href="{{ route('cars.edit', ["car"=>$car])}}" class="btn btn-sm btn-primary">Edit</a>
                    <form class="d-inline" action="{{ route('cars.delete', ["car"=>$car])}}" method="POST">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach                  
        </table>
        <div class="ml-3">
            {{ $cars->links() }}
        </div>
        {{-- <nav aria-label="Pagination">
            <ul class="pagination pagination-sm justify-content-center">
                <form class="form-inline" action="" method="POST" role="form">
                    <div class="form-group">
                        <label for="perPage">Items per page</label>
                        <select name="perPage" id="perPage" class="form-control form-control-sm ml-2 mr-2" onchange="this.form.submit()">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                        </select>
                    </div>
                </form>
                {{ $cars->links() }}
            </ul>
        </nav> --}}
    </div>       
@endsection